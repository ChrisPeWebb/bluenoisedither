﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Defiant
{
    /// <summary>
    /// Manages setting important global shader properties that should exist in every single rendering state. (editor or runtime)
    /// eg, dither noise
    /// </summary>
    #if UNITY_EDITOR
    [InitializeOnLoad]
    #endif
    public static class GlobalShaderPropertyManager
    {
        #if UNITY_EDITOR
        static GlobalShaderPropertyManager()
        {
            SetGlobalShaderProperties();
        }
        #endif


        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void OnBeforeSceneLoadRuntimeMethod()
        {
            SetGlobalShaderProperties();
        }

        private static Texture2D m_blueNoiseCrossfadeTexture;
        public static Texture2D BlueNoiseCrossfadeTexture
        {
            get
            {
                if (!m_blueNoiseCrossfadeTexture)
                    m_blueNoiseCrossfadeTexture = Resources.Load<Texture2D>("GlobalShaderProperties/BlueNoiseCrossfade_16x256");
                return m_blueNoiseCrossfadeTexture;
            }
        }


        public static void SetGlobalShaderProperties()
        {
            Shader.SetGlobalTexture("_BlueNoiseCrossfade", BlueNoiseCrossfadeTexture);
        }
    }
}
