﻿// Adding this in a surface shader replaces the default unity dither functionality on all passes with one that uses blue noise

#ifndef DEFIANT_DITHER_CROSSFADE_INCLUDED
#define DEFIANT_DITHER_CROSSFADE_INCLUDED

sampler2D _BlueNoiseCrossfade;
float4 _BlueNoiseCrossfade_TexelSize;

#ifdef LOD_FADE_CROSSFADE

// Undefine unity implementation and define our own
#undef UNITY_APPLY_DITHER_CROSSFADE
#define UNITY_APPLY_DITHER_CROSSFADE(vpos)  UnityApplyBlueNoiseDitherCrossFade(vpos)
void UnityApplyBlueNoiseDitherCrossFade(float2 vpos)
{
	vpos /= 16; // the dither mask texture is 16x16
	vpos.y = frac(vpos.y) * 0.0625 + unity_LODFade.y; // quantized lod fade by 16 levels
	clip(tex2D(_BlueNoiseCrossfade, vpos).r - 0.5);
}
#else
#define UNITY_APPLY_DITHER_CROSSFADE(vpos)
#endif

#endif // DEFIANT_DITHER_CROSSFADE_INCLUDED
